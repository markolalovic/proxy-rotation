# generate_requests

import requests
import time, random
import proxyscrape
import os
from datetime import datetime

url = 'http://test.markolalovic.com' # our website for testing
collector = proxyscrape.create_collector('default', 'http')

# define some user agents = devices
user_agents = {
    'macOS': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36',
    'iPhone': 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.4 Mobile/15E148 Safari/604.1',
    'Android': 'Mozilla/5.0 (Linux; Android 5.1; Lenovo A1010a20 Build/LMY47I) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.89 Mobile Safari/537.36',
    'Windows1': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',
    'Windows2': 'Mozilla/5.0 (Windows; U; Windows NT 6.0;en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6)',
}

min_wait = 1 # minimum amount of time between requests
max_wait = 2 # maximum amount of time between requests
sleep_time = random.randrange(min_wait, max_wait) # random sleep time

used_proxies = [] # to save used proxies
while True:
    date = str(datetime.now())

    user_agent = random.choice(list(user_agents.values())) # pick random user description e.g. iPhone
    headers = {'user-agent': user_agent}
    proxy = collector.get_proxy({'code': ('si', 'sk', 'hr', 'rs', 'ba', 'mk', 'it', 'at', 'hu')}) # from Slovenia, Slovakia, Croatia, ...
    p = proxy.host + ':' + proxy.port
    #print('Requesting trough ' + p)
    try:
        if p not in used_proxies: # so we don't repeat request using the same proxy
            r = requests.get(url=url, headers=headers, proxies={"http": p, "https": p}, timeout=10)
            used_proxies.append(p)
            fd = open('log_requests.csv', 'a')
            fd.write(date + ', ' + p + '\n') # write proxy IP address to log_requests
            fd.close()
    except:
        pass
        #print('Connection error')
    time.sleep(sleep_time) # sleep for random time (we are still generating flat distribution of requests)
