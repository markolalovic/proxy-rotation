# proxy-rotation

## Description
Goal is to generate traffic to our website from thousands of *users*, i.e. requests from unique IP addresses.

## Design
By launching a Python script `generate_requests.py` that sends a request to our website from proxies scraped from various sites which offer free HTTP, HTTPS, SOCKS4 and SOCKS5 proxies:

* anonymous-proxy
* free-proxy-list	 
* proxy-daily-http
* socks-proxy
* ssl-proxy
* uk-proxy
* us-proxy

from local region (Slovenia, Croatia, Italy, ...) using random user device description (e.g. iPhone), waits some random time and repeats. Scraping is done using `proxyscrape 0.3.0` library.

Time of request and proxy IP address is written to `log_requests.csv` file each time new request is sent.

Bash script `run_proxy_rotation.sh` to start Python script in the background in screen on a server.
